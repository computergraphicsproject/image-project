#include "sletester.h"
#include "sources/slesolver.h"
#include <QDir>
#include <fstream>
#include <QStringList>
#include <QMap>
#include <QTest>
#include <sstream>

const QStringList SLETester::tests = {
	"4 "
	"1 0 1 2 "
	"2 1 0 3 3 2 "
	"1 2 3 9 "
	"4 3 0 4 1 3 2 9 3 10 "
	"4 5 3 3 2 "
	"0.777778 2.5 -1.3272 0.33333",
	"4 "
	"1 0 0 1 "
	"1 1 1 2 "
	"1 2 2 3 "
	"1 3 3 4 "
	"4 2 2 2 2 "
	"2 1 0.6666667 0.5"
};

bool SLETester::comparePixelFloat(long double x, long double y)
{
    return fabs(x - y) < 0.5;
}

SLETester::SLETester(QObject *parent) :
    QObject(parent)
{
}

void SLETester::test()
{
    //std::ios_base::sync_with_stdio(false);
    //qDebug() << directory.entryList();
    foreach (QString x, tests)
    {
        //cin.open(testFolder.toStdString() + x.toStdString());
		QMap <QPair<int, int>, double> matrix;
        int n;
        std::stringstream cin;
        cin << x.toStdString();

        cin >> n;
        for (int i = 0; i < n; ++i)
        {
            int m;
            cin >> m;
            int row;
            cin >> row;
            //qDebug() << row;
            for (int j = 0; j < m; ++j)
            {
                int col; double data;
                cin >> col >> data;
                matrix[qMakePair(row, col)]= data;
            }
        }
        cin >> n;
        QVector <double> vec(n);
        for (int i = 0; i < n; ++i)
            cin >> vec[i];
        qDebug() << x;
        auto solution2 = SLESolver::BiCGStab(matrix, vec);
        //auto solution1 = SLESolver::solve(matrix, vec);
        QVector <double> solution1(n);
        for (int i = 0; i < n; ++i)
            cin >> solution1[i];
        for (int i = 0; i < n; ++i)
        {
            qDebug() << double(solution1[i]) << " " << double(solution2[i]);


            QVERIFY(comparePixelFloat(solution1[i], solution2[i]));
        }

        //cin.close();

    }
}
