#ifndef SLETESTER_H
#define SLETESTER_H

#include <QObject>
#include <QStringList>

class SLETester : public QObject
{
    Q_OBJECT
    bool comparePixelFloat(long double x, long double y);
	static const QStringList tests;
public:
    explicit SLETester(QObject *parent = 0);

signals:

private slots:
    void test();
};

#endif // SLETESTER_H
