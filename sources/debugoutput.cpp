#include "debugoutput.h"
#include <QPixmap>

void DebugOutput::outputImage(const QImage &rhs)
{
    QLabel *label = new QLabel;
    label->setPixmap(QPixmap::fromImage(rhs));
    label->show();
}

void DebugOutput::outputImage(int w, int h, const QMap<QPair<int, int>, int> &red, const QMap<QPair<int, int>, int> &green, const QMap<QPair<int, int>, int> &blue)
{
    QImage disp (w, h, QImage::Format_ARGB32);
    for (auto it = red.begin(); it != red.end(); ++it)
    {
        disp.setPixel(it.key().first, it.key().second, qRgb(qBound(0, it.value(), 255),
                      qBound(0, green[qMakePair(it.key().first, it.key().second)], 255),
                      qBound(0, blue[qMakePair(it.key().first, it.key().second)],255)));
    }
    DebugOutput::outputImage(disp);
}

void DebugOutput::outputQVector(const QVector<long double> &rhs)
{
    QVector <double> outVal;
    std::copy(rhs.cbegin(), rhs.cend(), std::back_inserter(outVal));
    qDebug() << outVal;
}
