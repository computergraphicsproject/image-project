#ifndef BASEPICTUREHOLDER_H
#define BASEPICTUREHOLDER_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPixmap>
#include <QImage>
#include <QGraphicsPixmapItem>
#include <QGraphicsLineItem>
#include <QLine>
#include <QPen>
#include <QBrush>

class BasePictureHolder : public QGraphicsView
{
    Q_OBJECT

	//selection routine
	bool isInitial = true;

	static const int selectionRed = 255;
	static const int selectionGreen = 205;
	static const int selectionBlue = 0;

	QGraphicsLineItem *temporarySelection = nullptr; //лінія, коли рухаємо мишкою при виборі області
	QGraphicsLineItem *temporaryConnector = nullptr;
	QVector <QPointF> selectionPolygon; //набір точок, які ми клікнули коли вибирали область виділення
	QVector <QGraphicsLineItem *> selectionPolygonLines; //лінії, що з'єднюють точки вище
	int currentSelectionNumber = 0; //скільки точок вже вибрали
	QPolygonF selectionPolygonF; //багатокутник, утворений з точкок, що задають область

	void clearSelection(); //очистка точок що вибрали
	void clearSelectionLines(); //видалення ліній
	void onFinalStep(); //на останньому кроці виділення
protected:
    //крок зміни масштабу при скролінгу
	bool selectionEnabled = false;

    const static double stepScaleFactor;

    QGraphicsScene *scene;
    QPixmap picture; //початкове зображення
    QImage src;
    QGraphicsPixmapItem *item; //зображення
    double scaleFactor = 1; //початковий масштаб
    virtual void wheelEvent(QWheelEvent *event);

	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
public:
    explicit BasePictureHolder(const QString &path, QWidget *parent = 0);
    virtual ~BasePictureHolder();
    virtual void open(const QString &path);
    virtual void setImage (const QImage &arg);
    virtual QImage getImage () const;
    virtual void display(const QImage &arg);
    virtual void display();
	virtual void triangulate(int precision) {};
    //explict BasePictureHolder()
    //BasePictureHolder& operator=(BasePictureHolder newPicture);

	//SELECTION routine
	void enableSelection(bool);
	void setIsInitial(bool);
	void onSelectionFinished();

signals:
	void selectionDone(const QImage &, int offsetX, int offsetY);
	void selectionUnDone();
	void openRequest();


};

#endif // BASEPICTUREHOLDER_H
