#ifndef RECIEVER_H
#define RECIEVER_H

#include "basepictureholder.h"
#include "filters/filters.h"
#include <QHash>
#include <QVector>
#include <QWheelEvent>
#include <memory>

class Reciever : public BasePictureHolder
{
    Q_OBJECT
    QGraphicsPixmapItem *recievedItem = nullptr;
    QImage recievedImage;
//    QList <QImage> appliedChanges;
//    QList <QImage>::iterator currentChange = appliedChanges.end();

    double currentRotation = 0.0;
    int currentBrightness = 0;
    int currentWarm = 0;
    int currentCool = 0;
    int currentSaturation = 0;

public:
    explicit Reciever(const QString &path, QWidget *parent = 0);

    virtual void open(const QString &path);
    void rotateLeft(double);
    void rotateRight(double);
	void removeRecievedItem();
	void triangulate(int precision);

	QImage getRecievedImage() const;
	QPoint getOffset() const;
    ~Reciever();
protected:
    virtual void wheelEvent(QWheelEvent *) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
signals:
    void tooLargeInsertion ();

public slots:
    void recieveImage (const QImage &);
    void saveImage (const QString &path);
    void insertSelection ();
};

#endif // RECIEVER_H
