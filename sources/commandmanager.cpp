#include "commandmanager.h"

CommandManager::CommandManager(BasePictureHolder *target, QObject *parent) :
    QObject(parent),
    managed (target)
{
}

void CommandManager::undo()
{
    if (!appliedChanges.empty() && currentChange != appliedChanges.begin())
    {
        --currentChange;
        managed->setImage((**currentChange).getImage());
        managed->display();
    }
}

void CommandManager::redo()
{
    if (!appliedChanges.empty() && currentChange + 1 != appliedChanges.end())
    {
        ++currentChange;
        managed->setImage((**currentChange).getImage());
        managed->display();
    }
}

void CommandManager::applyFilter(std::shared_ptr<BaseFilter> filter)
{
    filter->applyFilter();
    recentChange = filter;
    managed->display(filter->getImage());
}

void CommandManager::applyPartialFilter(std::shared_ptr <BaseFilter> filter, const QImage &cropped, int ox, int oy)
{
	filter->applyPartialFilter(cropped, ox, oy);
	recentChange = filter;
	managed->display(filter->getImage());
}

void CommandManager::fixItem()
{
    managed->setImage(recentChange->getImage());
    if (currentChange != appliedChanges.end())
        appliedChanges.erase(currentChange + 1, appliedChanges.end());
    appliedChanges.append(recentChange);
    currentChange = appliedChanges.end() - 1;
}
