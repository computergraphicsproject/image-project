#include "filterfactory.h"

FilterFactory::FilterFactory()
{
	knownClasses["Blur"] = Blur::staticMetaObject;
	knownClasses["Brightness"] = Brightness::staticMetaObject;
	knownClasses["Cool"] = Cool::staticMetaObject;
	knownClasses["Fixing"] = Fixing::staticMetaObject;
	knownClasses["Greyscale"] = Greyscale::staticMetaObject;
	knownClasses["MCM"] = MCM::staticMetaObject;
	knownClasses["Negative"] = Negative::staticMetaObject;
	knownClasses["Saturation"] = Saturation::staticMetaObject;
	knownClasses["Sepia"] = Sepia::staticMetaObject;
	knownClasses["Sharpen"] = Sharpen::staticMetaObject;
	knownClasses["Warm"] = Warm::staticMetaObject;
}

std::shared_ptr <BaseFilter> FilterFactory::createFilter(const QString &filterName, const QImage &imageArg, int deltaArg, QObject *parent) const
{
	if (!knownClasses.contains(filterName))
		throw std::logic_error("This filter is unknown");
	QMetaObject mo = knownClasses[filterName];
	QString data = mo.className();
	return std::shared_ptr <BaseFilter>(qobject_cast <BaseFilter *>
		(mo.newInstance(Q_ARG(QImage, imageArg), Q_ARG(int, deltaArg))));
}

QList <QString> FilterFactory::getClassList() const
{
	QList <QString> retVal = knownClasses.keys();
	qSort(retVal);
	return retVal;
}

QMetaObject FilterFactory::getMetaData(const QString &className) const
{
	if (!knownClasses.contains(className))
		throw std::logic_error("Trying to get metadata for non-existing class");
	return knownClasses[className];
}

FilterFactory::~FilterFactory()
{

}