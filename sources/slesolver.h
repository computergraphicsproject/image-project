#ifndef SLESOLVER_H
#define SLESOLVER_H
#include <QMap>
#include <QVector>

//#define TEST_CREATION

const int BLOCK_SIZE = 1024;
const int MAX_GRID_DIM = 1024;
const int MAX_ITEMS = 5;

struct SparseItem
{
	int col;
	double value;
};

class SLESolver
{
    static int test;
public:
    SLESolver() = delete;
    SLESolver (const SLESolver &) = delete;
	static QVector <double> solve(const QMap <QPair<int, int>, double> &A, const QVector<double> &B);
	static QVector <double> BiCGStab(const QMap <QPair<int, int>, double> &A, const QVector<double> &B);
	static QVector <double> BiCGStabCuda(const QMap <QPair<int, int>, double> &A, const QVector<double> &B);
};

#endif // SLESOLVER_H
