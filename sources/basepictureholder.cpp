#include "basepictureholder.h"
#include <QWheelEvent>
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <cmath>
#include <QScrollBar>
#include <qmath.h>
#include <QRectF>
#include <QLabel>
#include <QRegion>
#include <QPointF>

const double BasePictureHolder::stepScaleFactor = 0.05;
BasePictureHolder::BasePictureHolder(const QString &path, QWidget *parent) :
    QGraphicsView(parent)
{
    picture = QPixmap (path);
    scene = new QGraphicsScene;
    setScene(scene);
    item = scene->addPixmap(picture);
    item->setAcceptHoverEvents(true);
    setMouseTracking(true);
}

BasePictureHolder::~BasePictureHolder()
{
    delete item;
    item = nullptr;
    delete scene;
    scene = nullptr;
}

void BasePictureHolder::open(const QString &path)
{
	clearSelection();
	clearSelectionLines();

    picture = QPixmap (path);
    item->setPixmap(picture);

}

void BasePictureHolder::setImage(const QImage &arg)
{
    src = arg;
}

QImage BasePictureHolder::getImage() const
{
    return src;
}

void BasePictureHolder::display(const QImage &arg)
{
    picture = QPixmap::fromImage(arg);
    item->setPixmap(picture);
}

void BasePictureHolder::display()
{
    picture = QPixmap::fromImage(src);
    item->setPixmap(picture);
}

void BasePictureHolder::wheelEvent(QWheelEvent *event)
{
	//SELECTION routine
	if (isInitial)
	{
		event->ignore();
		return;
	}
	clearSelection();
	clearSelectionLines();
	//end of routine

    scaleFactor += copysign(stepScaleFactor, event->delta());
    if (scaleFactor < stepScaleFactor)
        scaleFactor += stepScaleFactor;
    if (scaleFactor > 10)
        scaleFactor -= stepScaleFactor;
    item->setPixmap(picture.scaled(picture.width()*scaleFactor,picture.height()*scaleFactor));
    setSceneRect(item->boundingRect());
    //qDebug () << sceneRect() << " " << item->boundingRect();
}


void BasePictureHolder::clearSelection()
{
	currentSelectionNumber = 0;
	selectionPolygon.clear();
	if (temporarySelection)
	{
		scene->removeItem(temporarySelection);
		delete temporarySelection;
		temporarySelection = nullptr;
	}
	if (temporaryConnector)
	{
		scene->removeItem(temporaryConnector);
		delete temporaryConnector;
		temporaryConnector = nullptr;
	}
}

void BasePictureHolder::clearSelectionLines()
{
	while (!selectionPolygonLines.empty())
	{
		scene->removeItem(selectionPolygonLines.first());
		delete selectionPolygonLines.first();
		selectionPolygonLines.pop_front();
	}
	emit selectionUnDone();
}

void BasePictureHolder::onFinalStep()
{
	selectionPolygonF = QPolygonF(selectionPolygon);
	clearSelection();
	onSelectionFinished();
}

void BasePictureHolder::onSelectionFinished()
{
	QRectF rec = selectionPolygonF.boundingRect();
	selectionPolygonF.translate(-rec.x(), -rec.y());
	qDebug() << rec;
	QImage tIm(rec.width(), rec.height(), QImage::Format_ARGB32);
	QImage src = picture.scaled(picture.width()*scaleFactor, picture.height()*scaleFactor).toImage().copy(rec.toRect());
	tIm.fill(Qt::transparent);
	//qDebug()<<(qAlpha(tIm.pixel(0,0)));
	QPainter p(&tIm);
	p.setClipRegion(QRegion(selectionPolygonF.toPolygon()));
	p.drawImage(0, 0, src);
	//    QLabel *boozywoozy = new QLabel;
	//    boozywoozy->setStyleSheet("background-color: green;");
	//    boozywoozy->setPixmap(QPixmap::fromImage(tIm));
	//    boozywoozy->show();
	emit selectionDone(tIm, rec.x(), rec.y());
}

void BasePictureHolder::enableSelection(bool arg)
{
	selectionEnabled = arg;
	if (!selectionEnabled)
	{
		clearSelection();
		clearSelectionLines();
	}
}

void BasePictureHolder::setIsInitial(bool val)
{
	isInitial = val;
}

void BasePictureHolder::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (selectionEnabled && currentSelectionNumber > 1)
	{

		selectionPolygonLines.push_back(
			scene->addLine(selectionPolygon[currentSelectionNumber - 1].x(),
			selectionPolygon[currentSelectionNumber - 1].y(),
			selectionPolygon[0].x(),
			selectionPolygon[0].y(),
			QPen(QBrush(QColor(selectionRed, selectionGreen, selectionBlue)), 4, Qt::DashLine, Qt::FlatCap, Qt::BevelJoin)));
		onFinalStep();
	}
}

void BasePictureHolder::mousePressEvent(QMouseEvent *event)
{
	if (isInitial)
	{
		emit openRequest();
	}
	else if (selectionEnabled)
	{
		if (item->boundingRect().contains(mapToScene(event->pos())))
		{
			if (currentSelectionNumber != 0)
			{
				QPointF temp;
				bool finalStep = false;
				if ((hypot(mapToScene(event->pos()).x() - selectionPolygon[0].x(),
					mapToScene(event->pos()).y() - selectionPolygon[0].y()) <= 5) ||
					((currentSelectionNumber > 1) &&
					(hypot(mapToScene(event->pos()).x() - selectionPolygon[currentSelectionNumber - 1].x(),
					mapToScene(event->pos()).y() - selectionPolygon[currentSelectionNumber - 1].y()) <= 5)))
				{
					temp = selectionPolygon[0];
					finalStep = true;
				}
				else
					temp = mapToScene(event->pos());
				selectionPolygonLines.push_back(
					scene->addLine(selectionPolygon[currentSelectionNumber - 1].x(),
					selectionPolygon[currentSelectionNumber - 1].y(),
					temp.x(),
					temp.y(),
					QPen(QBrush(QColor(selectionRed, selectionGreen, selectionBlue)), 4, Qt::DashLine, Qt::FlatCap, Qt::BevelJoin)));
				if (finalStep)
				{
					onFinalStep();
				}
				else
				{
					++currentSelectionNumber;
					selectionPolygon.push_back(temp);
				}
			}
			else
			{
				clearSelectionLines();
				selectionPolygon.push_back(mapToScene(event->pos()));
				++currentSelectionNumber;
			}
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void BasePictureHolder::mouseMoveEvent(QMouseEvent *event)
{
	if (selectionEnabled)
	{
		if (temporarySelection)
		{
			scene->removeItem(temporarySelection);
			delete temporarySelection;
			temporarySelection = nullptr;
		}

		if (temporaryConnector)
		{
			scene->removeItem(temporaryConnector);
			delete temporaryConnector;
			temporaryConnector = nullptr;
		}

		if (currentSelectionNumber != 0)
		{
			temporarySelection = scene->addLine(QLineF(mapToScene(event->pos()),
				selectionPolygon[currentSelectionNumber - 1]),
				QPen(QBrush(QColor(selectionRed, selectionGreen, selectionBlue)), 4, Qt::DashLine, Qt::FlatCap, Qt::BevelJoin));
		}

		if (currentSelectionNumber != 0 && currentSelectionNumber != 1)
		{
			temporaryConnector = scene->addLine(QLineF(mapToScene(event->pos()),
				selectionPolygon[0]),
				QPen(QBrush(QColor(selectionRed, selectionGreen, selectionBlue)), 4, Qt::DashLine, Qt::FlatCap, Qt::BevelJoin));
		}
	}
	QGraphicsView::mouseMoveEvent(event);
}
