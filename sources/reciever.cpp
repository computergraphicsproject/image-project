#include "reciever.h"
#include "slesolver.h"
#include "debugoutput.h"
#include "triangulator.h"
#include <QRgb>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>
#include <QLabel>
#include <QPointF>
#include <QPoint>
#include <QRectF>
#include <QFileDialog>
#include <memory>
#include <cmath>
#include <random>
#include <QVector>

void Reciever::insertSelection()
{
    qDebug() << "FOR MOTHER RUSSIA";
    if (!recievedItem)
        throw std::logic_error("This (inserting selection) must only be available if smth is selected");
    qDebug() << recievedImage.width() << recievedImage.height();
    for (int i = 0; i < recievedImage.width(); ++i)
        for (int j = 0; j < recievedImage.height(); ++j)
            if (qAlpha(recievedImage.pixel(i, j)))
                src.setPixel(recievedItem->mapToItem(item, i, j).toPoint(), recievedImage.pixel(i, j));
    item->setPixmap(QPixmap::fromImage(src));
    removeRecievedItem();
    currentRotation = 0;
}

void Reciever::recieveImage(const QImage &img)
{
    qDebug() << "HOORA";
    if (img.width() > item->boundingRect().width() ||
        img.height() > item->boundingRect().height())
    {
        emit tooLargeInsertion();
        return;
    }
    recievedImage = img;
    src = item->pixmap().toImage();
    if (recievedItem)
        insertSelection();
    recievedItem = scene->addPixmap(QPixmap::fromImage(img));
    recievedItem->setFlags(QGraphicsItem::ItemIsMovable);
}

Reciever::Reciever(const QString &path, QWidget *parent):
    BasePictureHolder(path, parent)
{
    item->setZValue(-100);
    src = item->pixmap().toImage();
}

void Reciever::removeRecievedItem()
{
	if (recievedItem)
	{
		scene->removeItem(recievedItem);
		delete recievedItem;
		recievedItem = nullptr;
	}
}

void Reciever::saveImage(const QString &path)
{
    src.save(path);
}


void Reciever::wheelEvent(QWheelEvent *e)
{

    BasePictureHolder::wheelEvent(e);
    //Check if recieved item is out of bounds
    //If so, scale back
    if (recievedItem &&
        !item->mapToScene(item->boundingRect()).boundingRect().contains(recievedItem->mapToScene(recievedItem->boundingRect()).boundingRect()))
    {
        scaleFactor += stepScaleFactor;

    }
    item->setPixmap(picture.scaled(picture.width()*scaleFactor,picture.height()*scaleFactor));
    setSceneRect(item->boundingRect());
    src = item->pixmap().toImage();
}

void Reciever::mouseMoveEvent(QMouseEvent *event)
{
	if (selectionEnabled)
		BasePictureHolder::mouseMoveEvent(event);
	else
	{
		if (recievedItem)
		{
			QPointF oldPos = recievedItem->pos();
			QGraphicsView::mouseMoveEvent(event);
			//Check if recievedItem is out of bounds
			//If so move it back
			if (!item->mapToScene(item->boundingRect()).boundingRect().contains(recievedItem->mapToScene(recievedItem->boundingRect()).boundingRect()))
				recievedItem->setPos(oldPos);
		}
		else
			QGraphicsView::mouseMoveEvent(event);
	}
}

void Reciever::mousePressEvent(QMouseEvent *event)
{

	BasePictureHolder::mousePressEvent(event);


}

void Reciever::open(const QString &path)
{
    BasePictureHolder::open(path);
    src = item->pixmap().toImage();
}

void Reciever::rotateLeft(double rot)
{
    //recievedItem->setTransformOriginPoint(100, 100);
    QTransform trans;
    double tX = recievedItem->pixmap().width() / 2.0, tY = recievedItem->pixmap().height() / 2.0;
    trans.translate(tX, tY).rotate(currentRotation -= rot).translate(-tX, -tY);
    recievedItem->setTransform(trans);

}

void Reciever::rotateRight(double rot)
{
    QTransform trans;
    double tX = recievedItem->pixmap().width() / 2.0, tY = recievedItem->pixmap().height() / 2.0;
    trans.translate(tX, tY).rotate(currentRotation += rot).translate(-tX, -tY);
    recievedItem->setTransform(trans);

}

QImage Reciever::getRecievedImage() const
{
	if (!recievedItem)
		throw std::logic_error("Recieved image request whils nothing is recieved");
	return recievedImage;
}

QPoint Reciever::getOffset() const
{
	if (!recievedItem)
		throw std::logic_error("Offset requested whilst nothing is recieved");
	return recievedItem->mapToItem(item, 0, 0).toPoint();
}

void Reciever::triangulate(int precision)
{
	double probability = precision / 100.0;
	if (probability < 0.01 || probability > 1.0)
		throw std::logic_error("Precision must be between 1 and 100");

	std::vector <QVector2D> chosenPoints;

	for (int i = 0; i < src.width(); ++i)
	{
		for (int j = 0; j < src.height(); ++j)
		{
			if ((i % 50 == 0) && (j % 50 == 0))
			{
				chosenPoints.push_back(QVector2D(i, j));
			}
		}
	}

	std::vector <QVector2D> result;
	Triangulator::Process(chosenPoints, result);

	QVector <QPoint> huina;
	huina.append(result[3 * 0].toPoint());
	huina.append(result[3 * 0 + 1].toPoint());
	huina.append(result[3 * 0 + 2].toPoint());
	scene->addPolygon(QPolygon(huina));

	for (int i = 0; i < result.size() / 3; ++i)
	{
		QVector <QPoint> huina;
		huina.append(result[3 * i].toPoint());
		huina.append(result[3 * i + 1].toPoint());
		huina.append(result[3 * i + 2].toPoint());
		scene->addPolygon(QPolygon(huina));

	}

}

//TODO: Write implementation
Reciever::~Reciever()
{

}


