#ifndef PDESOLVER_H
#define PDESOLVER_H
#include <QImage>
#include <QMap>
#include <QHash>
#include <QVector>

class PDESolver
{
private:

	QImage recievedImage;
	QImage src;
	QPoint offset;

	QMap <QPair<int, int>, double> sparseMatrix;
	QHash <QPair <int, int>, int> matrixPos;
	QVector <double> BRed;
	QVector <double> BGreen;
	QVector <double> BBlue;
	QMap <QPair<int, int>, int> sourceGradientRed;
	QMap <QPair<int, int>, int> sourceGradientGreen;
	QMap <QPair<int, int>, int> sourceGradientBlue;

	QMap <QPair<int, int>, int> recievedGradientRed;
	QMap <QPair<int, int>, int> recievedGradientGreen;
	QMap <QPair<int, int>, int> recievedGradientBlue;

	int countNeighbours(int x, int y);
	bool pixelOnEdge(int x, int y);
	bool pixelOutSide(int x, int y);
	void matrixFiller(int i, int j, int k, int l);
	void gradientFiller(int i, int j, int k, int l);
	void evaluteGradients();

public:
	PDESolver(const QImage &src, const QImage &recievedImage, const QPoint &offset);
	PDESolver& operator = (const PDESolver &) = delete;
	PDESolver(const QImage &src, const QImage &recievedImage);

	QImage solve();
};


#endif //PDESOLVER_H