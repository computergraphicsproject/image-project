#include "pdesolver.h"
#include "slesolver.h"
#include "debugoutput.h"

int PDESolver::countNeighbours(int x, int y)
{
	int cnt = 0;
	if (x - 1 >= 0)
		cnt += static_cast <bool> (qAlpha(recievedImage.pixel(x - 1, y)));
	if (y - 1 >= 0)
		cnt += static_cast <bool>(qAlpha(recievedImage.pixel(x, y - 1)));
	if (x + 1 <= recievedImage.width() - 1)
		cnt += static_cast <bool>(qAlpha(recievedImage.pixel(x + 1, y)));
	if (y + 1 <= recievedImage.height() - 1)
		cnt += static_cast <bool>(qAlpha(recievedImage.pixel(x, y + 1)));
	//qDebug() << cnt;
	return cnt;
}

bool PDESolver::pixelOnEdge(int x, int y)
{
	if ((0 == x) || (0 == y) || (recievedImage.width() - 1 == x) || (recievedImage.height() - 1 == y))
		return true;
	return ((!qAlpha(recievedImage.pixel(x - 1, y))) || (!qAlpha(recievedImage.pixel(x + 1, y))) ||
		(!qAlpha(recievedImage.pixel(x, y - 1))) || (!qAlpha(recievedImage.pixel(x, y + 1))));
}

bool PDESolver::pixelOutSide(int x, int y)
{
	return ((x < 0) || (y < 0) ||
		(x >= recievedImage.width()) ||
		(y >= recievedImage.height()) ||
		(!qAlpha(recievedImage.pixel(x, y))));
}

void PDESolver::matrixFiller(int i, int j, int k, int l)
{
	sparseMatrix[qMakePair(matrixPos[qMakePair(i, j)], matrixPos[qMakePair(i, j)])] = countNeighbours(i, j);
	if (!pixelOutSide(k, l))
	{
		if (!pixelOnEdge(k, l))
			sparseMatrix[qMakePair(matrixPos[qMakePair(i, j)], matrixPos[qMakePair(k, l)])] = -1.0;
		else
		{
			//            qDebug() << (qRed(src.pixel(recievedItem->mapToItem(item, k, l).toPoint()))) << " " <<
			//                        (qGreen(src.pixel(recievedItem->mapToItem(item, k, l).toPoint()))) << " " <<
			//                        (qBlue(src.pixel(recievedItem->mapToItem(item, k, l).toPoint())));
			BRed[matrixPos[qMakePair(i, j)]] += qRed(src.pixel(k + offset.x(), l + offset.y()));
			BGreen[matrixPos[qMakePair(i, j)]] += qGreen(src.pixel(k + offset.x(), l + offset.y()));
			BBlue[matrixPos[qMakePair(i, j)]] += qBlue(src.pixel(k + offset.x(), l + offset.y()));
		}

	}
}

void PDESolver::gradientFiller(int i, int j, int k, int l)
{
	if (!pixelOutSide(k, l))
	{
		recievedGradientRed[qMakePair(i, j)] += qRed(recievedImage.pixel(i, j)) - qRed(recievedImage.pixel(k, l));
		recievedGradientGreen[qMakePair(i, j)] += qGreen(recievedImage.pixel(i, j)) - qGreen(recievedImage.pixel(k, l));
		recievedGradientBlue[qMakePair(i, j)] += qBlue(recievedImage.pixel(i, j)) - qBlue(recievedImage.pixel(k, l));

		sourceGradientRed[qMakePair(i, j)] += qRed(src.pixel(i + offset.x(), j + offset.y()))
			- qRed(src.pixel(k + offset.x(), l + offset.y()));
		sourceGradientGreen[qMakePair(i, j)] += qGreen(src.pixel(i + offset.x(), j + offset.y()))
			- qGreen(src.pixel(k + offset.x(), l + offset.y()));
		sourceGradientBlue[qMakePair(i, j)] += qBlue(src.pixel(i + offset.x(), j + offset.y()))
			- qBlue(src.pixel(k + offset.x(), l + offset.y()));
	}
}

void PDESolver::evaluteGradients()
{
	for (int i = 0; i < recievedImage.width(); ++i)
	{
		for (int j = 0; j < recievedImage.height(); ++j)
		{
			if (qAlpha(recievedImage.pixel(i, j)))
			{
				gradientFiller(i, j, i + 1, j);
				gradientFiller(i, j, i - 1, j);
				gradientFiller(i, j, i, j + 1);
				gradientFiller(i, j, i, j - 1);
			}
		}
	}
	DebugOutput::outputImage(recievedImage.width(), recievedImage.height(),
		sourceGradientRed, sourceGradientGreen, sourceGradientBlue);

}


PDESolver::PDESolver(const QImage &src, const QImage &recievedImage, const QPoint &offset) :
src(src),
recievedImage(recievedImage),
offset(offset)
{ }

QImage PDESolver::solve()
{
	int k = 0;
	for (int i = 0; i < recievedImage.width(); ++i)
		for (int j = 0; j < recievedImage.height(); ++j)
			if (qAlpha(recievedImage.pixel(i, j)))
			{
		matrixPos[qMakePair(i, j)] = k;
		BRed.append(0);
		BGreen.append(0);
		BBlue.append(0);
		++k;
			}

	evaluteGradients();
	for (int i = 0; i < recievedImage.width(); ++i)
	{
		for (int j = 0; j < recievedImage.height(); ++j)
		{
			if (qAlpha(recievedImage.pixel(i, j)))
			{
				matrixFiller(i, j, i, j - 1);
				matrixFiller(i, j, i - 1, j);
				matrixFiller(i, j, i + 1, j);
				matrixFiller(i, j, i, j + 1);

				//if (std::pow(sourceGradientRed[qMakePair(i, j)], 2) + std::pow(sourceGradientGreen[qMakePair(i, j)], 2) + std::pow(sourceGradientBlue[qMakePair(i, j)], 2) >
				//    std::pow(recievedGradientRed[qMakePair(i, j)], 2) + std::pow(recievedGradientGreen[qMakePair(i, j)], 2) + std::pow(recievedGradientBlue[qMakePair(i, j)], 2))
				//{
				//    BRed[matrixPos[qMakePair(i, j)]] += sourceGradientRed[qMakePair(i, j)];
				//    BGreen[matrixPos[qMakePair(i, j)]] += sourceGradientGreen[qMakePair(i, j)];
				//    BBlue[matrixPos[qMakePair(i, j)]] += sourceGradientBlue[qMakePair(i, j)];

				//}
				//else
				{
					/*BRed[matrixPos[qMakePair(i, j)]] += recievedGradientRed[qMakePair(i, j)];
					BGreen[matrixPos[qMakePair(i, j)]] += recievedGradientGreen[qMakePair(i, j)];
					BBlue[matrixPos[qMakePair(i, j)]] += recievedGradientBlue[qMakePair(i, j)];*/

					double k1r = std::fabs(double(recievedGradientRed[qMakePair(i, j)])) / (std::abs(sourceGradientRed[qMakePair(i, j)]) + std::abs(recievedGradientRed[qMakePair(i, j)]) + 1);
					double k2r = 1 - k1r;

					double k1g = std::fabs(double(recievedGradientGreen[qMakePair(i, j)])) / (std::abs(sourceGradientGreen[qMakePair(i, j)]) + std::abs(recievedGradientGreen[qMakePair(i, j)]) + 1);
					double k2g = 1 - k1g;

					double k1b = std::fabs(double(recievedGradientBlue[qMakePair(i, j)])) / (std::abs(sourceGradientBlue[qMakePair(i, j)]) + std::abs(recievedGradientBlue[qMakePair(i, j)]) + 1);
					double k2b = 1 - k1b;

					BRed[matrixPos[qMakePair(i, j)]] += k1r * recievedGradientRed[qMakePair(i, j)] + k2r * sourceGradientRed[qMakePair(i, j)];
					BGreen[matrixPos[qMakePair(i, j)]] += k1g * recievedGradientGreen[qMakePair(i, j)] + k2g * sourceGradientGreen[qMakePair(i, j)];
					BBlue[matrixPos[qMakePair(i, j)]] += k1b * recievedGradientBlue[qMakePair(i, j)] + k2b * sourceGradientBlue[qMakePair(i, j)];


				}
				//if (std::abs(sourceGradientRed[qMakePair(i, j)]) > std::abs(recievedGradientRed[qMakePair(i, j)]))
				//	BRed[matrixPos[qMakePair(i, j)]] += sourceGradientRed[qMakePair(i, j)];
				//else
				//	BRed[matrixPos[qMakePair(i, j)]] += recievedGradientRed[qMakePair(i, j)];
				//if ( std::abs(sourceGradientGreen[qMakePair(i, j)]) > std::abs(recievedGradientGreen[qMakePair(i, j)]))
				//	BGreen[matrixPos[qMakePair(i, j)]] += sourceGradientGreen[qMakePair(i, j)];
				//else
				//	BGreen[matrixPos[qMakePair(i, j)]] += recievedGradientGreen[qMakePair(i, j)];
				//if (std::abs(sourceGradientBlue[qMakePair(i, j)]) > std::abs(recievedGradientBlue[qMakePair(i, j)]))
				//	BBlue[matrixPos[qMakePair(i, j)]] += sourceGradientBlue[qMakePair(i, j)];
				//else
				//	BBlue[matrixPos[qMakePair(i, j)]] += recievedGradientBlue[qMakePair(i, j)];
			}
		}
	}
	//SLESolver::solve(sparseMatrix, BRed);
	//SLESolver::solve(sparseMatrix, BGreen);
	//SLESolver::solve(sparseMatrix, BBlue);

	//SLESolver::BiCGStab(sparseMatrix, BRed);
	//SLESolver::BiCGStab(sparseMatrix, BGreen);
	//SLESolver::BiCGStab(sparseMatrix, BBlue);

	QVector <double> ARed = SLESolver::BiCGStabCuda(sparseMatrix, BRed);
	QVector <double> AGreen = SLESolver::BiCGStabCuda(sparseMatrix, BGreen);
	QVector <double> ABlue = SLESolver::BiCGStabCuda(sparseMatrix, BBlue);
	k = 0;
	for (int i = 0; i < recievedImage.width(); ++i)
	{
		for (int j = 0; j < recievedImage.height(); ++j)
		{
			//qDebug() << i << " " << j << " " << qAlpha(recievedImage.pixel(i,j));
			if (qAlpha(recievedImage.pixel(i, j)))
			{
				//qDebug() << i << " " << j;
				//qDebug() << ARed[k] << " " << AGreen[k] << " " << ABlue[k];
				ARed[k] = fmax(ARed[k], 0); ARed[k] = fmin(ARed[k], 255);
				AGreen[k] = fmax(AGreen[k], 0); AGreen[k] = fmin(AGreen[k], 255);
				ABlue[k] = fmax(ABlue[k], 0); ABlue[k] = fmin(ABlue[k], 255);

				src.setPixel(
					i + offset.x(), j + offset.y(),
					qRgb(std::round(ARed[k]), std::round(AGreen[k]), std::round(ABlue[k])));
				++k;
			}
		}
	}
	return src;
}