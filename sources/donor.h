#ifndef DONOR_H
#define DONOR_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPixmap>
#include <QPointF>
#include <QVector>
#include <QGraphicsLineItem>
#include <cmath>
#include <QPolygonF>
#include "basepictureholder.h"
#include <QRgb>

class Donor : public BasePictureHolder
{
	Q_OBJECT
public:
	explicit Donor(const QString &path, QWidget *parent = 0);

};

#endif // DONOR_H
