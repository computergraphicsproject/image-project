#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

#include <QObject>
#include "basepictureholder.h"
#include "filters/basefilter.h"
#include <QList>
#include <memory>

class CommandManager : public QObject
{
    Q_OBJECT
    BasePictureHolder *managed = nullptr;

    //undo and redo
    QList <std::shared_ptr<BaseFilter>> appliedChanges;
    QList <std::shared_ptr<BaseFilter>>::iterator currentChange = appliedChanges.end();
    std::shared_ptr <BaseFilter> recentChange;

public:
    CommandManager(BasePictureHolder *target, QObject *parent = 0);

signals:

public slots:
    void undo();
    void redo();
    void applyFilter (std::shared_ptr <BaseFilter> filter);
	void applyPartialFilter(std::shared_ptr <BaseFilter> filter, const QImage &cropped, int ox, int oy);
    void fixItem();
};

#endif // COMMANDMANAGER_H
