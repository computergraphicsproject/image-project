#ifndef ABSTRACTFACTORY_H
#define ABSTRACTFACTORY_H

#include <QObject>
#include <QString>
#include <memory>
#include "filters/filters.h"
#include <QMetaObject>

class AbstractFactory
{
public:
	virtual std::shared_ptr<BaseFilter> createFilter(const QString &className, const QImage &imageArg, int deltaArg = 0, QObject *parent = 0) const = 0;
	virtual QList <QString> getClassList() const = 0;
	virtual QMetaObject getMetaData(const QString &className) const = 0;
	virtual ~AbstractFactory();
};

#endif //ABSTRACTFACTORY_H