#ifndef FILTERFACTORY_H
#define FILTERFACTORY_H
#include "abstractfactory.h"
#include "filters/filters.h"
#include <QHash>
#include <QMetaObject>

class FilterFactory: public AbstractFactory
{
public:
	FilterFactory();
	virtual std::shared_ptr<BaseFilter> createFilter(const QString &filterName, const QImage &imageArg, int deltaArg = 0, QObject *parent = 0) const;
	virtual QList <QString> getClassList() const override;
	virtual QMetaObject getMetaData(const QString &className) const override;
	~FilterFactory();
protected:
	QHash <QString, QMetaObject> knownClasses;
};

#endif // FILTERFACTORY_H