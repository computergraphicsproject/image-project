#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include "slesolver.h"
#include <cublas_v2.h>
#include <QTime>
#include <QLabel>
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

#define gpuCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

__global__ void addVectors(const double * const lhs, const double * const rhs, double * const res, const unsigned n)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n)
		res[i] = lhs[i] + rhs[i];
}

__global__ void substractVectors(const double * const lhs, const double * const rhs, double * const res, const unsigned n)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n)
		res[i] = lhs[i] - rhs[i];
}

__global__ void multVectorByInt(const double * const lhs, const double * const rhs, double * const res, const unsigned n)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n)
		res[i] = rhs[i] * *lhs;
}

__global__ void multVectorByMatrix(const SparseItem * const lhs, const double * const rhs, double * const res, const unsigned n)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < n)
	{
		res[i] = 0;
		for (int j = i * MAX_ITEMS; j < i * MAX_ITEMS + MAX_ITEMS; ++j)
			if (lhs[j].col != -1)
				res[i] += lhs[j].value * rhs[lhs[j].col];
	}
}



__global__ void initValue(double * const d_var, const double val)
{
	*d_var = val;
}

__global__ void calcBetai(const double * const d_roii, const double * const d_alphai,
	const double * const d_roi, const double * const d_omegai, double * const d_betai)
{
	*d_betai = (*d_roii * *d_alphai) / (*d_roi * *d_omegai);
}

__global__ void calcAlphai(const double * const d_roi, const double * const d_tempi, double * const d_alphai)
{
	*d_alphai = *d_roi / *d_tempi;
}

__global__ void calcOmegai(const double * const d_tempi, const double * const d_tempii, double * const d_omegai)
{
	*d_omegai = *d_tempi / *d_tempii;
}

__global__ void calcIterDiff(const double * const lhs, const double * const rhs, const int * const ind, double * const res)
{
	*res = fabs(lhs[*ind] - rhs[*ind]);
}

QVector<double> SLESolver::BiCGStabCuda(const QMap <QPair<int, int>, double> &A, const QVector<double> &B)
{
	QTime benchmark;
	benchmark.start();

	cublasHandle_t h;
	cublasCreate(&h);
	cublasSetPointerMode(h, CUBLAS_POINTER_MODE_DEVICE);
	unsigned n = B.size();

	unsigned gridDim = n / BLOCK_SIZE + (bool)(n % BLOCK_SIZE);
	SparseItem *h_A, *d_A;
	h_A = (SparseItem *)malloc(n * MAX_ITEMS * sizeof(SparseItem));
	cudaMalloc((void **)&d_A, n * MAX_ITEMS * sizeof(SparseItem));
	unsigned i = 0;
	QMap <QPair<int, int>, double>::const_iterator iter = A.begin();
	for (unsigned j = 0; j < n; ++j)
	{
		for (; iter.key().first == j; ++iter)
		{
			h_A[i].col = iter.key().second;
			h_A[i].value = iter.value();
			++i;
		}
		while ((j + 1) * MAX_ITEMS > i)
		{
			h_A[i].col = -1;
			h_A[i].value = 0;
			++i;
		}
	}

	cudaMemcpy(d_A, h_A, n * MAX_ITEMS * sizeof(SparseItem), cudaMemcpyHostToDevice);


	double *h_B = (double *)malloc(n * sizeof(double)), *d_B;
	cudaMalloc((void **)&d_B, n * sizeof(double));
	for (unsigned i = 0; i < n; ++i)
		h_B[i] = B[i];
	cudaMemcpy(d_B, h_B, n * sizeof(double), cudaMemcpyHostToDevice);

	double *d_xi;
	cudaMalloc((void **)&d_xi, n * sizeof(double));
	cudaMemset(d_xi, 0, n * sizeof(double));

	double *d_xii;
	cudaMalloc((void **)&d_xii, n * sizeof(double));
	cudaMemset(d_xii, 0, n * sizeof(double));

	double *d_ti;
	cudaMalloc((void **)&d_ti, n * sizeof(double));
	cudaMemset(d_ti, 0, n * sizeof(double));

	double *d_si;
	cudaMalloc((void **)&d_si, n * sizeof(double));
	cudaMemset(d_si, 0, n * sizeof(double));

	double *d_ri;
	cudaMalloc((void **)&d_ri, n * sizeof(double));

	double *d_temp1;
	cudaMalloc((void **)&d_temp1, n * sizeof(double));

	double *d_temp2;
	cudaMalloc((void **)&d_temp2, n * sizeof(double));

	double *d_temp3;
	cudaMalloc((void **)&d_temp3, n * sizeof(double));

	cudaMemset(d_temp1, 0, sizeof(double) * n);
	multVectorByMatrix <<<gridDim, BLOCK_SIZE >>>(d_A, d_xi, d_temp1, n);
	substractVectors <<<gridDim, BLOCK_SIZE >>>(d_B, d_temp1, d_ri, n);


	double *d_rii;
	cudaMalloc((void **)&d_rii, n * sizeof(double));
	cudaMemcpy(d_rii, d_ri, n * sizeof(double), cudaMemcpyDeviceToDevice);

	double *d_pi;
	cudaMalloc((void **)&d_pi, n * sizeof(double));
	cudaMemset(d_pi, 0, n * sizeof(double));

	double *d_vi;
	cudaMalloc((void **)&d_vi, n * sizeof(double));
	cudaMemset(d_vi, 0, n * sizeof(double));

	double *d_tempRes;
	cudaMalloc((void **)&d_tempRes, BLOCK_SIZE * sizeof(double));
	cudaMemset(d_tempRes, 0, BLOCK_SIZE * sizeof(double));

	double *d_tempRes2;
	cudaMalloc((void **)&d_tempRes2, BLOCK_SIZE * sizeof(double));
	cudaMemset(d_tempRes2, 0, BLOCK_SIZE * sizeof(double));

	double *d_alphai, *d_omegai, *d_roi, *d_roii, *d_betai, *d_tempi, *d_tempii;
	cudaMalloc(&d_alphai, sizeof(double));
	cudaMalloc(&d_omegai, sizeof(double));
	cudaMalloc(&d_roi, sizeof(double));
	cudaMalloc(&d_roii, sizeof(double));
	cudaMalloc(&d_betai, sizeof(double));
	cudaMalloc(&d_tempi, sizeof(double));
	cudaMalloc(&d_tempii, sizeof(double));
	initValue <<<1, 1 >>>(d_alphai, 1.0);
	initValue <<<1, 1 >>>(d_omegai, 1.0);
	initValue <<<1, 1 >>>(d_roi, 1.0);

	// = d_alphai = d_omegai = 1;

	const double eps = 1e-6;
	const double epsomega = 1e-6;
	unsigned int iterCount = 0;

	double *h_omegai = (double *)malloc(sizeof(double));
	double *h_normB = (double *)malloc(sizeof(double));
	double *d_normB;
	cudaMalloc(&d_normB, sizeof(double));
	cublasDnrm2_v2(h, n, d_B, 1, d_normB);
	cudaMemcpy(h_normB, d_normB, sizeof(double), cudaMemcpyDeviceToHost);
	double *h_norm = (double *)malloc(sizeof(double));
	double *d_norm;
	cudaMalloc(&d_norm, sizeof(double));
	double *h_diff = (double *)malloc(sizeof(double));
	int *d_ind;
	cudaMalloc(&d_ind, sizeof(int));
	double *d_diff;
	cudaMalloc(&d_diff, sizeof(double));
	
	for (;;)
	{
		++iterCount;
		//break condition
		cudaMemcpy(h_omegai, d_omegai, sizeof(double), cudaMemcpyDeviceToHost);
		cublasDnrm2_v2(h, n, d_ri, 1, d_norm);
		cudaMemcpy(h_norm, d_norm, sizeof(double), cudaMemcpyDeviceToHost);
		if ((fabs(*h_omegai) < epsomega) || (*h_norm / *h_normB < eps))
			break;
		//calculating roii
		cublasDdot_v2(h, n, d_rii, 1, d_ri, 1, d_roii);
		//
		//calculating beatai & roi

		calcBetai <<<1, 1 >>>(d_roii, d_alphai, d_roi, d_omegai, d_betai);
		cudaMemcpy(d_roi, d_roii, sizeof(double), cudaMemcpyDeviceToDevice);
		//

		//calculating pi
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_omegai, d_vi, d_temp1, n);
		substractVectors <<<gridDim, BLOCK_SIZE >>>(d_pi, d_temp1, d_temp2, n);
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_betai, d_temp2, d_temp3, n);
		addVectors <<<gridDim, BLOCK_SIZE >>>(d_temp3, d_ri, d_pi, n);

		//

		//calculating vi
		multVectorByMatrix <<<gridDim, BLOCK_SIZE >>>(d_A, d_pi, d_vi, n);

		//

		//calculating aplhai
		cublasDdot_v2(h, n, d_rii, 1, d_vi, 1, d_tempi);
		calcAlphai <<<1, 1 >>>(d_roi, d_tempi, d_alphai);
		//

		//calculating si
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_alphai, d_vi, d_temp1, n);
		substractVectors <<<gridDim, BLOCK_SIZE >>>(d_ri, d_temp1, d_si, n);
		//

		//calculating ti
		multVectorByMatrix <<<gridDim, BLOCK_SIZE >>>(d_A, d_si, d_ti, n);
		//


		//calculating omegai
		cublasDdot_v2(h, n, d_ti, 1, d_si, 1, d_tempi);
		cublasDdot_v2(h, n, d_ti, 1, d_ti, 1, d_tempii);
		calcOmegai <<<1, 1 >>>(d_tempi, d_tempii, d_omegai);

		//calculating xii
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_omegai, d_si, d_temp1, n);
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_alphai, d_pi, d_temp2, n);
		addVectors <<<gridDim, BLOCK_SIZE >>>(d_temp1, d_temp2, d_temp3, n);
		addVectors <<<gridDim, BLOCK_SIZE >>>(d_xi, d_temp3, d_xii, n);

		//iteration Difference break condition
		substractVectors<<<gridDim, BLOCK_SIZE>>>(d_xi, d_xii, d_temp1, n);
		cublasIdamax_v2(h, n, d_temp1, 1, d_ind);
		calcIterDiff<<<1,1>>>(d_xii, d_xi, d_ind, d_diff);
		cudaMemcpy(h_diff, d_diff, sizeof(double), cudaMemcpyDeviceToHost);
		if (*h_diff < eps)
			break;
		cudaMemcpy(d_xi, d_xii, n * sizeof(double), cudaMemcpyDeviceToDevice);
		//


		//calculating ri
		multVectorByInt <<<gridDim, BLOCK_SIZE >>>(d_omegai, d_ti, d_temp1, n);
		substractVectors <<<gridDim, BLOCK_SIZE >>>(d_si, d_temp1, d_ri, n);
		//

	}
	cublasDestroy(h);
	double *h_xi = (double *)malloc(n * sizeof(double));
	cudaMemcpy(h_xi, d_xi, n * sizeof(double), cudaMemcpyDeviceToHost);
	QVector <double> retVal(n);
	for (int i = 0; i < n; ++i)
		retVal[i] = h_xi[i];

	cudaFree(d_alphai);
	cudaFree(d_omegai);
	cudaFree(d_roi);
	cudaFree(d_roii);
	cudaFree(d_betai);
	cudaFree(d_tempi);
	cudaFree(d_tempii);

	cudaFree(d_xi);
	cudaFree(d_xii);
	cudaFree(d_ri);
	cudaFree(d_vi);
	cudaFree(d_pi);
	cudaFree(d_si);
	cudaFree(d_ti);
	cudaFree(d_rii);
	cudaFree(d_temp1);
	cudaFree(d_temp2);
	cudaFree(d_temp3);
	cudaFree(d_tempRes);
	cudaFree(d_tempRes2);
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_norm);
	cudaFree(d_normB);
	cudaFree(d_ind);
	cudaFree(d_diff);

	free(h_diff);
	free(h_A);
	free(h_B);
	free(h_norm);
	free(h_normB);
	free(h_omegai);
	free(h_xi);

	int millisec = benchmark.elapsed();
	QLabel *cudaTest = new QLabel(QString::number(millisec));
	cudaTest->show();
	return retVal;
}
