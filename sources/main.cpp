#include "mainwindow.h"
#include "slesolver.h"
#include <QApplication>
#include <QDebug>
#include "tests/sletester.h"
#include <QTest>
#include <cuda.h>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //QTest::qExec(new SLETester, argc, argv);
    QApplication::setStyle("fusion");
    MainWindow w;
    w.show();

    return a.exec();
}
