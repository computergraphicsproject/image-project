#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QWidgetAction>
#include <QString>
#include <QByteArray>
#include <QFile>
#include "pdesolver.h"

void MainWindow::enableAllControls()
{
	for (QSlider *item : filterSliders.keys())
	{
		item->setEnabled(1);
		filterSliders[item]->setEnabled(1);
	}
	for (QPushButton *item : filterButtons)
	{
		item->setEnabled(1);
	}
	degSelector->setEnabled(1);
}

void MainWindow::enableSelectionControls()
{
	ui->actionBlend->setEnabled(1);
	ui->actionFix->setEnabled(1);
	ui->actionLeftRotate->setEnabled(1);
	ui->actionRightRotate->setEnabled(1);
	degSelector->setEnabled(1);
}

void MainWindow::disableSelectionControls()
{
	ui->actionBlend->setEnabled(0);
	ui->actionFix->setEnabled(0);
	ui->actionLeftRotate->setEnabled(0);
	ui->actionRightRotate->setEnabled(0);
	degSelector->setEnabled(0);

}

void MainWindow::nullSliders()
{
	for (QSlider *item : filterSliders.keys())
	{
		item->blockSignals(1);
		item->setValue(0);
		item->blockSignals(0);
	}
	disableOKs();
}

void MainWindow::noBlockNullSliders()
{
	for (QSlider *item : filterSliders.keys())
		item->setValue(0);
}

void MainWindow::disableOKs()
{
	for (QPushButton *item : filterSliders.values())
		item->setDisabled(1);
}

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	setWindowTitle("Poisson PhotoBungalo");
	filterFactory = new FilterFactory;
	generateControls();
	splitter = new QSplitter(this);
	splitter->setOrientation(Qt::Horizontal);
	donorView = new Donor(":/images/initOpenSource.png", this);
	splitter->addWidget(donorView);


	//this widget is needed as a spacer in the toolbar
	QWidget *emptyWidget = new QWidget();
	emptyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

	ui->toolBar->insertWidget(ui->actionSelectReciever, emptyWidget);

	recieverView = new Reciever(":/images/initOpenTarget.png", this);
	splitter->addWidget(recieverView);

	degSelector = new QDoubleSpinBox(this);
	degSelector->setMinimum(0.0);
	degSelector->setMaximum(360.0);
	degSelector->setSingleStep(0.1);
	QWidgetAction *degAction = new QWidgetAction(this);
	degAction->setDefaultWidget(degSelector);
	ui->toolBar->insertAction(ui->actionRightRotate, degAction);


	ui->horizontalLayout->addWidget(splitter);
	ui->horizontalLayout->setStretchFactor(ui->controlsLayout, 2);
	ui->horizontalLayout->setStretchFactor(splitter, 8);

	degAction->setDisabled(1);
	ui->actionSelectDonor->setDisabled(1);
	ui->actionSelectReciever->setDisabled(1);
	ui->actionInsert->setDisabled(1);
	ui->actionBlend->setDisabled(1);
	ui->actionFix->setDisabled(1);
	ui->actionLeftRotate->setDisabled(1);
	ui->actionRightRotate->setDisabled(1);
	disableOKs();

	recieverManager = new CommandManager(recieverView);

	centralWidget()->setLayout(ui->horizontalLayout);

	connect(ui->actionBlend, &QAction::triggered, [this]()
	{
		noBlockNullSliders();
		disableOKs();
		disableSelectionControls();
		recieverManager->applyFilter(std::make_shared<Fixing>(Fixing(
			PDESolver(recieverView->getImage(), recieverView->getRecievedImage(), recieverView->getOffset()).solve())));
		recieverManager->fixItem();
		recieverView->removeRecievedItem();
	});
	connect(ui->actionFix, &QAction::triggered, [this]()
	{
		noBlockNullSliders();
		disableOKs();
		disableSelectionControls();
		recieverView->insertSelection();
	});
	connect(donorView, &BasePictureHolder::selectionDone, [this](const QImage &img)
	{
		imageReadyDonor = img;
		ui->actionInsert->setEnabled(1);

	});

	connect(recieverView, &BasePictureHolder::selectionDone, [this](const QImage &img, int ox, int oy)
	{
		imageReadyReciever = img;
		filterState = FilterState::PART_OF_IMAGE;
		recieverOffsetX = ox;
		recieverOffsetY = oy;
	});

	connect(recieverView, &BasePictureHolder::selectionUnDone, [this]()
	{
		filterState = FilterState::FULL_IMAGE;
	}
	);

	connect(donorView, &Donor::selectionUnDone, [this](){ui->actionInsert->setEnabled(0); });
	connect(ui->actionInsert, &QAction::triggered, [this](){
		noBlockNullSliders();
		disableOKs();
		enableSelectionControls();
		ui->actionSelectReciever->setChecked(false);
		recieverView->recieveImage(imageReadyDonor);
		recieverManager->applyFilter(std::make_shared<Fixing>(Fixing(recieverView->getImage())));

	});
	connect(recieverView, &Reciever::tooLargeInsertion, [this](){
		QMessageBox::warning(this, "Warning", "Too large region is selected");
		ui->actionBlend->setDisabled(true); });
		connect(ui->actionUndo, &QAction::triggered, this, &MainWindow::noBlockNullSliders);
		connect(ui->actionUndo, &QAction::triggered, recieverManager, &CommandManager::undo);
		connect(ui->actionRedo, &QAction::triggered, this, &MainWindow::noBlockNullSliders);
		connect(ui->actionRedo, &QAction::triggered, recieverManager, &CommandManager::redo);
		connect(ui->actionSelectDonor, &QAction::toggled, donorView, &Donor::enableSelection);
		connect(ui->actionSelectReciever, &QAction::toggled, recieverView, &Reciever::enableSelection);
		connect(ui->actionSelectReciever, &QAction::toggled, recieverView, &Reciever::removeRecievedItem);

		connect(donorView, &Donor::openRequest, this, &MainWindow::on_actionOpenSI_triggered);
		connect(recieverView, &Reciever::openRequest, this, &MainWindow::on_actionOpenTI_triggered);


}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_actionAbout_triggered()
{

}

void MainWindow::on_actionOpenSI_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
		"/home",
		tr("Images (*.png *.bmp *.jpg *.jpeg)"));
	if (!fileName.isEmpty())
	{
		donorView->open(fileName);
		donorOpened = true;
		donorView->setIsInitial(false);
	}
	if (donorOpened && recieverOpened)
		ui->actionSelectDonor->setEnabled(1);
}

void MainWindow::on_actionOpenTI_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
		"/home",
		tr("Images (*.png *.bmp *.jpg *.jpeg)"));
	if (!fileName.isEmpty())
	{
		recieverView->open(fileName);
		recieverOpened = true;
		recieverView->setIsInitial(false);
		enableAllControls();
		recieverManager->applyFilter(std::make_shared<Fixing>(Fixing(recieverView->getImage())));
		recieverManager->fixItem();
		ui->actionSelectReciever->setEnabled(1);
		recieverView->triangulate(50);
	}
	if (donorOpened && recieverOpened)
		ui->actionSelectDonor->setEnabled(1);
}

void MainWindow::on_actionSave_As_triggered()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Open File"),
		"/home",
		tr("Images (*.png *.bmp *.jpg)"));
	if (fileName.isEmpty())
		return;
	recieverView->saveImage(fileName);
}

void MainWindow::on_actionLeftRotate_triggered()
{
	recieverView->rotateLeft(degSelector->value());
}

void MainWindow::on_actionRightRotate_triggered()
{
	recieverView->rotateRight(degSelector->value());
}

void MainWindow::on_Slider_valueChanged(int value)
{
	nullSliders();
	QSlider *senderSlider = qobject_cast <QSlider *>(sender());
	filterSliders[senderSlider]->setEnabled(1);
	senderSlider->blockSignals(1);
	senderSlider->setValue(value);
	senderSlider->blockSignals(0);
	applyProperFilter(filterFactory->createFilter(senderSlider->objectName(), recieverView->getImage(), value));
}




void MainWindow::onOkClicked()
{
	nullSliders();
	recieverManager->fixItem();
}

void MainWindow::applyProperFilter(std::shared_ptr<BaseFilter> arg)
{
	if (filterState == FULL_IMAGE)
	{
		recieverManager->applyFilter(arg);
	}
	else if (filterState == PART_OF_IMAGE)
	{
		recieverManager->applyPartialFilter(arg, imageReadyReciever, recieverOffsetX, recieverOffsetY);
	}
}

void MainWindow::on_filterButton_clicked()
{
	QPushButton *button = qobject_cast <QPushButton *>(sender());
	applyProperFilter(filterFactory->createFilter(button->objectName(), recieverView->getImage()));
	recieverManager->fixItem();
}

void MainWindow::generateControls()
{
	QList <QString> filters = filterFactory->getClassList();
	slidersLayout = new QVBoxLayout;
	buttonsLayout = new QVBoxLayout;

	for (QString item : filters)
	{
		QMetaObject mo = filterFactory->getMetaData(item);
		int isDisplayable = mo.indexOfClassInfo("displayable");
		if (!(isDisplayable == -1 || QString(mo.classInfo(isDisplayable).value()) != QString("false")))
			continue;
		int isAdjustable = mo.indexOfClassInfo("adjustable");
		if (isAdjustable == -1)
			throw std::logic_error(std::string("Class ") + item.toStdString() + std::string(" must contain metainfo \'adjustable\'"));
		if (QString(mo.classInfo(isAdjustable).value()) == QString("false"))
		{
			QPushButton *button = new QPushButton(item, this);
			button->setObjectName(item);
			connect(button, &QPushButton::clicked, this, &MainWindow::on_filterButton_clicked);
			button->setDisabled(1);
			buttonsLayout->addWidget(button);
			filterButtons.append(button);
		}
		else if (QString(mo.classInfo(isAdjustable).value()) == QString("true"))
		{
			slidersLayout->addItem(new QSpacerItem(5, 10));
			slidersLayout->addWidget(new QLabel(item, this));
			QHBoxLayout *tempLayout = new QHBoxLayout;
			QSlider *slider = new QSlider(Qt::Horizontal, this);
			slider->setMinimum(FILTER_SLIDER_MIN);
			slider->setMaximum(FILTER_SLIDER_MAX);
			slider->setObjectName(item);
			connect(slider, &QSlider::valueChanged, this, &MainWindow::on_Slider_valueChanged);
			slider->setDisabled(1);
			QPushButton *okButton = new QPushButton("OK", this);
			connect(okButton, &QPushButton::clicked, this, &MainWindow::onOkClicked);
			okButton->setDisabled(1);

			tempLayout->addWidget(slider);
			tempLayout->addWidget(okButton);
			slidersLayout->addLayout(tempLayout);
			filterSliders[slider] = okButton;
		}
		else
		{
			throw std::logic_error("adjustable meta information must be either \'true\' or \'false\'");
		}
	}

	ui->controlsLayout->addLayout(slidersLayout);
	ui->controlsLayout->addLayout(buttonsLayout);
}