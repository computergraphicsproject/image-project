#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "donor.h"
#include "reciever.h"
#include "commandmanager.h"
#include "filters/filters.h"
#include <QSplitter>
#include <QDoubleSpinBox>
#include "filterfactory.h"
#include <QSlider>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpacerItem>
#include <QMetaObject>
#include <QMetaClassInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Donor *donorView;
    Reciever *recieverView;
    CommandManager *recieverManager;
	FilterFactory *filterFactory;
	QVBoxLayout *slidersLayout;
	QVBoxLayout *buttonsLayout;

    QGraphicsScene *scene;
    QSplitter *splitter;
    QDoubleSpinBox *degSelector;
    QImage imageReadyDonor;

	QImage imageReadyReciever;
	int recieverOffsetX;
	int recieverOffsetY;
	
	//struct SliderControl
	//{
	//	QSlider *slider;
	//	QPushButton *okButton;
	//};
	//QList <SliderControl> filterSliders;
	QList <QPushButton *> filterButtons;
	QHash <QSlider *, QPushButton *> filterSliders;

	static const int FILTER_SLIDER_MIN = -100;
	static const int FILTER_SLIDER_MAX = 100;

    bool donorOpened = false;
    bool recieverOpened = false;
    void enableAllControls();
    void enableSelectionControls ();
    void disableSelectionControls();
    void nullSliders();
    void noBlockNullSliders();
    void disableOKs ();
	void applyProperFilter(std::shared_ptr<BaseFilter> arg);
	void generateControls();

	enum FilterState { FULL_IMAGE, PART_OF_IMAGE } filterState = FULL_IMAGE;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void undoRequest ();
    void redoRequest ();

private slots:
    void on_actionAbout_triggered();

    void on_actionOpenSI_triggered();

    void on_actionOpenTI_triggered();

    void on_actionSave_As_triggered();

    void on_actionLeftRotate_triggered();

    void on_actionRightRotate_triggered();

    void onOkClicked ();

	void on_filterButton_clicked();

	void on_Slider_valueChanged(int value);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
