#ifndef DEBUGOUTPUT_H
#define DEBUGOUTPUT_H
#include <QDebug>
#include <QImage>
#include <QLabel>
#include <QVector>


class DebugOutput
{
public:
    DebugOutput() = delete;
    DebugOutput(const DebugOutput &) = delete;
    DebugOutput& operator = (const DebugOutput &) = delete;

    static void outputImage (const QImage &rhs);
    static void outputImage(int w, int h, const QMap <QPair<int, int>, int> &red,
                            const QMap <QPair<int, int>, int> &green, const QMap <QPair<int, int>, int> &blue);
    static void outputQVector (const QVector <long double> &rhs);
};

#endif // DEBUGOUTPUT_H
