#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include "slesolver.h"
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h> 

__global__ void multVectorByMatrix(SparseItem *lhs, double *rhs, double *res)
{
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	for (unsigned int j = i * 5; j < i * 5 + 5; ++j)
		if (lhs[j].col != -1)
			res[i] = lhs[j].value * rhs[i];
}

QVector<double> SLESolver::BiCGStabCuda(const QMap<int, QMap<int, double> > &A, const QVector<double> &B)
{

	unsigned int n = B.size();
	int gridDim = n / BLOCK_SIZE + (bool)(n % BLOCK_SIZE);
	
	thrust::device_vector<double> d_B(B.toStdVector());
	thrust::host_vector<SparseItem> h_A(n * 5);
	unsigned int i = 0;
	for (int j = 0; j < n; ++j)
	{
		for (QMap <int, double>::const_iterator iter = A[j].cbegin; iter != A[j].cend(); ++iter)
		{
			h_A[i].col = iter.key();
			h_A[i].value = iter.value();
			++i;
		}
		while ((j + 1) * 5 - 1 > i)
		{
			h_A[i].col = -1;
			++i;
		}
	}

	thrust::device_vector<SparseItem> d_A = h_A;
	thrust::device_vector<double> xi(n, 0);
	thrust::device_vector<double> ri(n);
	thrust::device_vector<double> temp;

}
