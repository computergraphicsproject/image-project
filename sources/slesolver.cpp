#include "slesolver.h"
#include <stdexcept>
#include <QPair>
#include <QDebug>
#ifdef QT_DEBUG
#include <fstream>
#endif
#include <QTime>
#include <QLabel>
int SLESolver::test = 7;

namespace SLE
{
    //WARNING: this operator is for dot product
    double operator *(const QVector<double> &lhs, const QVector<double> &rhs)
    {
        if (lhs.size() != rhs.size())
            throw std::logic_error("Vectors size must be equal");
        double retVal = 0;
        for (int i = 0; i < lhs.size(); ++i)
            retVal += rhs[i] * lhs[i];
        return retVal;
    }

    double norm (const QVector <double> &rhs)
    {
        double nm = 0;
        for (double it: rhs)
            nm += it * it;
        return sqrt(nm);
    }

	QVector<double> operator *(const QMap <QPair<int, int>, double> &lhs, const QVector<double> &rhs)
    {
        QVector <double> retVal (rhs.size(), 0);
		for (QMap <QPair<int, int>, double>::const_iterator it = lhs.cbegin(); it != lhs.cend(); ++it)
            retVal [it.key().first] += rhs[it.key().second] * it.value();
        return retVal;
    }

    QVector <double> operator *(double x, const QVector <double> &rhs)
    {
        QVector <double> retVal(rhs.size());
        for (int i = 0; i < rhs.size(); ++i)
            retVal[i] = rhs[i] * x;
        return retVal;
    }

    QVector <double> operator + (const QVector <double> &lhs, const QVector <double> &rhs)
    {
        if (lhs.size() != rhs.size())
            throw std::logic_error("Vectors size must be equal");
        QVector <double> retVal(rhs.size());
        for (int i = 0; i < rhs.size(); ++i)
            retVal[i] = lhs[i] + rhs[i];
        return retVal;
    }

    QVector <double> operator - (const QVector <double> &lhs, const QVector <double> &rhs)
    {
        if (lhs.size() != rhs.size())
            throw std::logic_error("Vectors size must be equal");
        QVector <double> retVal(rhs.size());
        for (int i = 0; i < rhs.size(); ++i)
            retVal[i] = lhs[i] - rhs[i];
        return retVal;
    }

	QMap <QPair<int, int>, double> rotate(const QMap <QPair<int, int>, double> &rhs)
    {
		QMap <QPair<int, int>, double> retVal;
		for (QMap <QPair<int, int>, double>::const_iterator it = rhs.cbegin(); it != rhs.cend(); ++it)
            retVal[qMakePair(it.key().second, it.key().first)] = it.value();
        return retVal;
    }

    double iterationDifference (const QVector <double> &lhs, const QVector <double> &rhs)
    {
        double maxDiff = 0;
        for (int i = 0; i < lhs.size(); ++i)
            if (fabs(lhs[i] - rhs[i]) > maxDiff)
                maxDiff = fabs(lhs[i] - rhs[i]);
        return maxDiff;
    }
}

QVector<double> SLESolver::solve(const QMap <QPair<int, int>, double> &A, const QVector <double> &B)
{
    using namespace SLE;
	QTime benchmark;
	benchmark.start();
    auto AT = rotate(A);
    QVector <double> xi (B.size(), 0);
    QVector <double> xii;
    QVector <double> ri (B.size());
    for (int i = 0; i < B.size(); ++i)
        ri[i] = B[i];
    QVector <double> pi = ri;
    QVector <double> zi = ri;
    QVector <double> si = ri;
    const int iterations = 1500;
    const double eps = 0.0001;

    for (int i = 0; i < iterations; ++i)
    {
        double alphai = (pi * ri) / (si * (A * zi));
        xii = SLE::operator +(xi,  alphai * zi);
        if (iterationDifference(xi, xii) < eps)
        {
            qDebug() << "on iteration " << i << A.size();
            xi.swap(xii);
            break;
        }
        xi.swap(xii);
        QVector <double> rii = ri - alphai * (A * zi);
        QVector <double> pii = pi - alphai * (AT * si);
        double betai = (pii * rii) / (pi * ri);
        zi = SLE::operator +(rii,  betai * zi);
        si = SLE::operator +(pii,  betai * si);
        pi.swap(pii);
        ri.swap(rii);
    }
	int millisec = benchmark.elapsed();
	QLabel *label = new QLabel(QString::number(millisec));
	label->show();
    return xi;
}

QVector<double> SLESolver::BiCGStab(const QMap <QPair<int, int>, double> &A, const QVector<double> &B)
{
    using namespace SLE;
#ifdef TEST_CREATION
    std::ofstream cout ("E:\\Projects\\tryImg\\testcases\\" + std::to_string(test) + ".test");
    cout << A.size() << "\n";
    for (QMap <int, QMap <int, double> >::const_iterator it = A.cbegin(); it != A.cend(); ++it)
    {
        cout << it->size() << "\n";
        cout << it.key() << "\n";
        for (QMap <int, double>::const_iterator iter = it->cbegin(); iter != it->cend(); ++iter)
                cout << iter.key() << " " << *iter << "\n";
    }
    cout << B.size() << "\n";
    for (QVector <double>::const_iterator it = B.cbegin(); it != B.cend(); ++it)
        cout << *it << "\n";
    ++test;
#endif

	QTime benchmark;
	benchmark.start();

    QVector <double> xi (B.size(), 0);
    QVector <double> ri = B - A * xi;
    QVector <double> rii = ri;
    double alphai, omegai;
    double roi = alphai = omegai = 1;
    QVector <double> pi (B.size(), 0);
    QVector <double> vi (B.size(), 0);
    const double eps = 1e-6;
    const double epsomega = 1e-6;
    double normB = norm(B);
    int iterCount = 0;
    for (;;)
    {
        ++iterCount;
        if (fabs(omegai) < epsomega || (norm(ri) / normB < eps))
            break;
        double roii = rii * ri;
        double betai = (roii * alphai) / (roi * omegai);
        roi = roii;
        pi = SLE::operator +(ri, betai * (pi - omegai * vi));
        vi = A * pi;
        alphai = roi / (rii * vi);
        QVector <double> si = ri - alphai * vi;
        QVector <double> ti = A * si;
		double t1 = ti * si;
		double t2 = ti * ti;
        omegai = (t1) / (t2);
        QVector <double> xii = SLE::operator +(xi, SLE::operator +(omegai * si, alphai * pi));
        if (iterationDifference(xii, xi) < eps)
            break;
        xi = xii;
        ri = si - omegai * ti;
    }

	int millisec = benchmark.elapsed();
	QLabel *label = new QLabel(QString::number(B.size()) + QString(" ") + QString::number(millisec));
	label->show();

    return xi;
}
