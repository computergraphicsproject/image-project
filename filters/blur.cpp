#include "blur.h"

QImage Blur::filteredImage(const QImage &arg, int delta) const
{
	QImage src = arg;

    int kernel[5][5]= {{0,0,1,0,0},
                       {0,1,3,1,0},
                       {1,3,7,3,1},
                       {0,1,3,1,0},
                       {0,0,1,0,1}};
    int kernelSize = 5;
    int sumKernel = 27;
    int r,g,b;
    QColor color;
    QColor oldColor;

    for(int x=kernelSize/2; x<src.width()-(kernelSize/2); x++)
    {
        for(int y=kernelSize/2; y<src.height()-(kernelSize/2); y++)
        {
            r=0;
            g=0;
            b=0;
            oldColor=QColor(src.pixel(x,y));
            for(int i = -kernelSize/2; i<= kernelSize/2; i++)
            {
                for(int j = -kernelSize/2; j<= kernelSize/2; j++)
                {
                    color = QColor(src.pixel(x+i, y+j));
                    r += color.red()*kernel[kernelSize/2+i][kernelSize/2+j];
                    g += color.green()*kernel[kernelSize/2+i][kernelSize/2+j];
                    b += color.blue()*kernel[kernelSize/2+i][kernelSize/2+j];
                }
            }
            r = qBound(0, r/sumKernel, 255);
            g = qBound(0, g/sumKernel, 255);
            b = qBound(0, b/sumKernel, 255);
            src.setPixel(x,y, qRgba(r,g,b,oldColor.alpha()));


        }
    }
    return src;
}

Blur::Blur(const QImage &arg, int delta) : BaseFilter(arg, delta) {}