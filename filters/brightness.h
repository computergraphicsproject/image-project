#ifndef BRIGHTNESS_H
#define BRIGHTNESS_H
#include "basefilter.h"
#include <QImage>

class Brightness : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "true")

	virtual QImage filteredImage(const QImage &arg, int delta) const override;
public:
	Q_INVOKABLE Brightness(const QImage &arg, int delta = 0);
    

};

#endif // BRIGHTNESS_H
