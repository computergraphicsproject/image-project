#ifndef FIXING_H
#define FIXING_H
#include "basefilter.h"

class Fixing : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")
	Q_CLASSINFO("displayable", "false")
	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Fixing(const QImage &arg, int delta = 0);
	
};

#endif // FIXING_H
