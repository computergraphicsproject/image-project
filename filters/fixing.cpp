#include "fixing.h"

QImage Fixing::filteredImage(const QImage &arg, int delta) const
{
    qWarning() << "You should not apply changes for Fixing";
    return arg;
}

Fixing::Fixing(const QImage &arg, int delta) : BaseFilter(arg, delta) {}