#ifndef FILTERS_H
#define FILTERS_H
#include "filters/basefilter.h"
#include "filters/blur.h"
#include "filters/brightness.h"
#include "filters/cool.h"
#include "filters/fixing.h"
#include "filters/negative.h"
#include "filters/saturation.h"
#include "filters/sepia.h"
#include "filters/sharpen.h"
#include "filters/warm.h"
#include "filters/greyscale.h"
#include "filters/mcm.h"
#endif // FILTERS_H
