#ifndef SATURATION_H
#define SATURATION_H
#include "basefilter.h"

class Saturation : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "true")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Saturation(const QImage &arg, int delta = 0);
	
};

#endif // SATURATION_H
