#ifndef COOL_H
#define COOL_H
#include "basefilter.h"

class Cool : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "true")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Cool(const QImage &arg, int delta = 0);
	
};

#endif // COOL_H
