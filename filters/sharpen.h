#ifndef SHARPEN_H
#define SHARPEN_H
#include "basefilter.h"

class Sharpen : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Sharpen(const QImage &arg, int delta = 0);
	
};

#endif // SHARPEN_H
