#include "brightness.h"
#include <QDebug>
#include <QColor>

QImage Brightness::filteredImage(const QImage &arg, int delta = 0) const
{
    QImage src = arg;
    QColor oldColor;
    int r,g,b;
    for(int x = 0; x < src.width(); ++x)
    {
        for(int y = 0; y < src.height(); y++)
        {
            oldColor = QColor(src.pixel(x,y));

            r = oldColor.red() + delta;
            g = oldColor.green() + delta;
            b = oldColor.blue() + delta;

            r = qBound(0, r, 255);
            g = qBound(0, g, 255);
            b = qBound(0, b, 255);
            src.setPixel(x,y,qRgba(r,g,b,oldColor.alpha()));
        }
    }
     return src;
}


Brightness::Brightness(const QImage &arg, int delta) : BaseFilter(arg, delta) {}