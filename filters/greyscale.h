#ifndef GREYSCALE_H
#define GREYSCALE_H
#include "basefilter.h"

class Greyscale : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Greyscale(const QImage &arg, int delta = 0);
	virtual void applyFilterCuda() override;
};

#endif // GREYSCALE_H
