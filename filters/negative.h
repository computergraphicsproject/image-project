#ifndef NEGATIVE_H
#define NEGATIVE_H
#include "basefilter.h"

class Negative : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Negative(const QImage &arg, int delta = 0);

};

#endif // NEGATIVE_H
