#include "greyscale.h"

__global__ void cudaGreyscale(uchar4* image, int numRows, int numCols)
{
	int i = (blockIdx.x * gridDim.y + blockIdx.y) * blockDim.x * blockDim.y + blockDim.y * threadIdx.x + threadIdx.y;
	int total = numRows * numCols;
	if (i < total)
	{
		int R = image[i].x;
		int G = image[i].y;
		int B = image[i].z;
		image[i].x = .299f * R + .587f * G + .114f * B;
		image[i].y = .299f * R + .587f * G + .114f * B;
		image[i].z = .299f * R + .587f * G + .114f * B;

	}
}

void Greyscale::applyFilterCuda()
{
	BaseFilter::imageToCuda();
	BaseFilter::applyFilterCuda();
	cudaGreyscale <<<dim3(numRows / threadsPerBlock.x + 1, numCols / threadsPerBlock.y + 1, 1), threadsPerBlock >>>(d_image, numRows, numCols);
	BaseFilter::cudaToImage();
}