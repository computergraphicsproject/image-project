#include "saturation.h"


QImage Saturation::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    QColor newColor;
    int h,s,l;
    qDebug()<<"Saturation starts";
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
             oldColor = QColor(src.pixel(x,y));
             newColor = oldColor.toHsl();

             h = newColor.hue();
             s = newColor.saturation()+delta;
             l = newColor.lightness();

             s = qBound(0, s, 255);
             newColor.setHsl(h, s, l);
             src.setPixel(x, y, qRgba(newColor.red(), newColor.green(), newColor.blue(),oldColor.alpha()));

        }

    }
    qDebug()<<"Saturation ends";
    return src;
}

Saturation::Saturation(const QImage &arg, int delta) : BaseFilter(arg, delta) {}