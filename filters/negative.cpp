#include "negative.h"

QImage Negative::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    int r;
    int g;
    int b;
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
             oldColor = QColor(src.pixel(x,y));
             r=255-oldColor.red();
             g=255-oldColor.green();
             b=255-oldColor.blue();

             src.setPixel(x,y,qRgb(r,g,b));
        }
    }

    return src;
}

Negative::Negative(const QImage &arg, int delta) : BaseFilter(arg, delta) {}