#ifndef SEPIA_H
#define SEPIA_H
#include "basefilter.h"

class Sepia : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Sepia(const QImage &arg, int delta = 0);
	
};

#endif // SEPIA_H
