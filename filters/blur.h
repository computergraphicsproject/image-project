#ifndef BLUR_H
#define BLUR_H
#include "basefilter.h"

class Blur : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "false")
	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Blur(const QImage &arg, int delta = 0);
	
};

#endif // BLUR_H
