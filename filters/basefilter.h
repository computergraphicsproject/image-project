#ifndef BASEFILTER_H
#define BASEFILTER_H
#include <QImage>
#include <QColor>
#include <QDebug>
#include <QObject>
#include <stdio.h>
#include <QtCore>

#include <cuda.h>
#include <cuda_runtime.h>

class BaseFilter: public QObject
{
	Q_OBJECT
	
	virtual QImage filteredImage(const QImage &arg, int delta = 0) const = 0;
	QImage curImage;

protected:
	enum RegionOfApplication {FULL_IMAGE, PART_OF_IMAGE};
	RegionOfApplication mode;

    int delta;

	//following fields are only required for CUDA run
	uchar4 *h_image, *d_image;
	int numRows;
	int numCols;
	int size;
	static const dim3 threadsPerBlock;
	//end of cuda required fields
public:
	BaseFilter(const BaseFilter &);
    BaseFilter(const QImage &arg, int delta = 0);
    virtual void applyFilter ();
	virtual void applyPartialFilter(const QImage &cropped, int offsetX, int offsetY);
	virtual QImage getImage() final;

	virtual void applyFilterCuda();
	virtual void cudaToImage();
	virtual void imageToCuda();    
    //virtual ~BaseFilter();
};

#endif // BASEFILTER_H
