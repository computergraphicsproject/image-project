#include "sepia.h"

QImage Sepia::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
             oldColor = QColor(src.pixel(x,y));
             //double average = (oldColor.Red()+oldColor.Green()+oldColor.Blue())/3;
             int outputRed = qBound(0,static_cast <int>((oldColor.red() * .393) + (oldColor.green() *.769) + (oldColor.blue() * .189)),255);
             int outputGreen = qBound(0,static_cast <int>((oldColor.red() * .349) + (oldColor.green() *.686) + (oldColor.blue() * .168)),255);
             int outputBlue = qBound(0,static_cast <int>((oldColor.red() * .272) + (oldColor.green() *.534) + (oldColor.blue() * .131)),255);
//             qBound(0,outputRed,255);
//             qBound(0,outputGreen,255);
//             qBound(0,outputBlue,255);

             src.setPixel(x,y,qRgb(outputRed,outputGreen,outputBlue));
        }
    }

    return src;
}

Sepia::Sepia(const QImage &arg, int delta) : BaseFilter(arg, delta) {}