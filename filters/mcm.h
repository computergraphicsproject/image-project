#ifndef MCM_H
#define MCM_H
#include "basefilter.h"
#include <QImage>

class MCM : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "true")

	virtual QImage filteredImage(const QImage &arg, int delta) const override;
public:
	Q_INVOKABLE MCM(const QImage &arg, int delta = 0);
	void heat(double *, double *, int i, int i_prev, int i_next, int j, int j_prev, int j_next) const;
	void convert(double *, double *, const QImage &curImage) const;
	void fds_mcm(double *, double *) const;
private:

	double        t_g = 4;                       /* gradient threshold */
	double        dt = 0.1;                      /* time step */




};

#endif // MCM_H
