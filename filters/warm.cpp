#include "warm.h"

QImage Warm::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    int r,g,b;
    qDebug()<<"Warm starts";
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
            oldColor = QColor(src.pixel(x,y));

            r = oldColor.red() + delta;
            g = oldColor.green() + delta;
            b = oldColor.blue();

            r = qBound(0, r, 255);
            g = qBound(0, g, 255);

            src.setPixel(x,y,qRgba(r,g,b,oldColor.alpha()));
        }
    }
     qDebug()<<"warm ends";

     return src;
     //changeImage();
}

Warm::Warm(const QImage &arg, int delta) : BaseFilter(arg, delta) {}