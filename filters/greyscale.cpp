#include "greyscale.h"

QImage Greyscale::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    qDebug() << "greyScale starts";
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
             oldColor = QColor(src.pixel(x,y));
             int average = (oldColor.red()+oldColor.green()+oldColor.blue())/3;

             src.setPixel(x,y,qRgba(average,average,average,oldColor.alpha()));
        }
    }
    return src;
}


Greyscale::Greyscale(const QImage &arg, int delta) : BaseFilter(arg, delta) {}