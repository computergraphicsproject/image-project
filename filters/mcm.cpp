#include "mcm.h"

#include<QMessageBox>

QImage MCM::filteredImage(const QImage &arg, int delta) const
{
	QImage curImage = arg;

	double *ptr_in = new double[numRows * numCols];
	double *ptr_out = new double[numRows * numCols];
	int R = delta;
	double n_iter_f = (R * R) / (2 * dt);

	convert(ptr_in, ptr_out, arg);
	//QMessageBox::information(this->,QString::number(n_iter_f),QString::number(n_iter_f));
	for (int i = 0; i<n_iter_f; i++)
	{
		qDebug() << "iteration " << i << endl;
		fds_mcm(ptr_in, ptr_out);
		// curImage =  newImage;
		for (int j = 0; j<numCols*numRows; ++j)
		{
			ptr_in[j] = ptr_out[j];
		}

	}
	for (int i = 0; i<numRows; ++i)
	{
		for (int j = 0; j<numCols; ++j)
		{
			int intense = ptr_in[i * numCols + j];
			intense = qBound(0, intense, 255);
			curImage.setPixel(j, i, qRgb(intense, intense, intense));

		}
	}

	delete[] ptr_in;
	delete[] ptr_out;
	return curImage;
}

MCM::MCM(const QImage &arg, int delta) : BaseFilter(arg, delta)

{


}

void MCM::heat(double *ptr_in, double *ptr_out, int i, int i_prev, int i_next, int j, int j_prev, int j_next) const
{
	double laplacian;
	laplacian = -4.0 * (*(ptr_in + i*numCols + j)) +
		*(ptr_in + i_prev*numCols + j) + *(ptr_in + i*numCols + j_prev) +
		*(ptr_in + i*numCols + j_next) + *(ptr_in + i_next*numCols + j);

	*(ptr_out + i*numCols + j) = *(ptr_in + i*numCols + j) + 0.5 * dt * laplacian;

}

void MCM::convert(double *ptr_in, double *ptr_out, const QImage &curImage) const
{

	for (int i = 0; i<numRows; ++i)
	{
		for (int j = 0; j<numCols; ++j)
		{
			ptr_in[i*numCols + j] = qRed(curImage.pixel(j, i));
		}
	}


}

void MCM::fds_mcm(double *ptr_in, double *ptr_out) const
{

	int  i, j, i_next, i_prev, j_next, j_prev;
	double         grad, s_x, s_y, lambda0, lambda1, lambda2, lambda3, lambda4;

	/* iterate on j and i, following the array order */
	for (i = 0; i<numRows; i++) 
	{

		/* symmetry for borders */
		i_next = (i<numRows - 1 ? i + 1 : i);
		i_prev = (i>0 ? i - 1 : i);

		for (j = 0; j<numCols; j++) 
		{

			/* symmetry for borders */
			j_next = (j<numCols - 1 ? j + 1 : j);
			j_prev = (j>0 ? j - 1 : j);

			/* computation of s_x, s_y and gradient */
			s_x = 2 * (*(ptr_in + i*numCols + j_next) - *(ptr_in + i*numCols + j_prev)) +
				*(ptr_in + i_prev*numCols + j_next) - *(ptr_in + i_prev*numCols + j_prev) +
				*(ptr_in + i_next*numCols + j_next) - *(ptr_in + i_next*numCols + j_prev);
			s_y = 2 * (*(ptr_in + i_next*numCols + j) - *(ptr_in + i_prev*numCols + j)) +
				*(ptr_in + i_next*numCols + j_next) - *(ptr_in + i_prev*numCols + j_next) +
				*(ptr_in + i_next*numCols + j_prev) - *(ptr_in + i_prev*numCols + j_prev);
			grad = 0.125 * sqrt((s_x*s_x) + (s_y*s_y));

			if (grad > t_g)
			{
				/* MCM diffusion */
				grad *= 8;   /* i.e. grad=\sqrt{s_x^2 + s_y^2} */
				lambda0 = 0.5 - ((s_x*s_x*s_y*s_y) / (pow(grad, 4)));
				lambda1 = 2 * lambda0 - ((s_x*s_x) / (grad*grad));
				lambda2 = 2 * lambda0 - ((s_y*s_y) / (grad*grad));
				lambda3 = -lambda0 + 0.5 * (1 - ((s_x*s_y) / (grad*grad)));
				lambda4 = -lambda0 + 0.5 * (1 + ((s_x*s_y) / (grad*grad)));

				*(ptr_out + i*numCols + j) = *(ptr_in + i*numCols + j) + dt * (
					-4 * lambda0 * (*(ptr_in + i*numCols + j)) +
					lambda1 * (*(ptr_in + i*numCols + j_next) +
					*(ptr_in + i*numCols + j_prev)) +
					lambda2 * (*(ptr_in + i_next*numCols + j) +
					*(ptr_in + i_prev*numCols + j)) +
					lambda3 * (*(ptr_in + i_prev*numCols
					+ j_prev) +
					*(ptr_in + i_next*numCols
					+ j_next)) +
					lambda4 * (*(ptr_in + i_prev*numCols
					+ j_next) +
					*(ptr_in + i_next*numCols
					+ j_prev)));
			}

			else
				/* Heat Equation */
				heat(ptr_in, ptr_out, i, i_prev, i_next, j, j_prev, j_next);
		}
	}
}






