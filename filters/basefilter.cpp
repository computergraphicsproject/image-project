#include "basefilter.h"
#include <QRgb>

const dim3 BaseFilter::threadsPerBlock = dim3(16, 16, 1);

BaseFilter::BaseFilter(const QImage &arg, int delta):
    curImage(arg),
    delta(delta),
	mode(FULL_IMAGE)
{
	size = numCols * numRows * sizeof(uchar4);
	numRows = arg.height();
	numCols = arg.width();
	curImage = curImage.convertToFormat(QImage::Format_ARGB32);
	/*for (int i = 0; i < numRows; ++i)
		for (int j = 0; j < numCols; ++j)
			curImage.setPixel(j, i, qRgb(0,0,0));*/
}

BaseFilter::BaseFilter(const BaseFilter &rhs)
{
	curImage = rhs.curImage;
	mode = rhs.mode;
	delta = rhs.delta;
}

void BaseFilter::applyFilter()
{
	curImage = filteredImage(curImage, delta);
}

void BaseFilter::applyPartialFilter(const QImage &cropped, int offsetX, int offsetY)
{

	QImage transformedCropped = filteredImage(cropped, delta);
	//TODO: fix issue with alpha channel
	for (int x = 0; x < transformedCropped.width(); ++x)
	{
		for (int y = 0; y < transformedCropped.height(); ++y)
		{
			if (qAlpha(cropped.pixel(x, y)))
				curImage.setPixel(x + offsetX, y + offsetY, transformedCropped.pixel(x, y));

		}
	}

}

QImage BaseFilter::getImage()
{
	return this->curImage;
}


void BaseFilter::imageToCuda()
{
	h_image = (uchar4 *)malloc(size);
	for (int i = 0; i < numRows; ++i)
	{
		for (int j = 0; j < numCols; ++j)
		{
			h_image[i * numCols + j].x = qRed(curImage.pixel(j, i));
			h_image[i * numCols + j].y = qGreen(curImage.pixel(j, i));
			h_image[i * numCols + j].z = qBlue(curImage.pixel(j, i));
		}
	}
}

void BaseFilter::cudaToImage()
{
	cudaMemcpy(h_image, d_image, size, cudaMemcpyDeviceToHost);
	for (int i = 0; i < numRows; ++i)
	{
		for (int j = 0; j < numCols; ++j)
		{
			unsigned char R = h_image[i * numCols + j].x;
			unsigned char G = h_image[i * numCols + j].y;
			unsigned char B = h_image[i * numCols + j].z;
			curImage.setPixel(j, i, qRgb(R, G, B));
		}
	}
}

void BaseFilter::applyFilterCuda()
{
	imageToCuda();
	cudaMalloc((void **)&this->d_image, size);
	cudaMemcpy(d_image, h_image, size, cudaMemcpyHostToDevice);
}
