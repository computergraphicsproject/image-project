#include "cool.h"

QImage Cool::filteredImage(const QImage &arg, int delta) const
{
    QImage src = arg;
    QColor oldColor;
    int r,g,b;
    qDebug()<<"Cool starts";
    qDebug() << delta;
    for(int x = 0; x<src.width(); x++)
    {
        for(int y = 0; y<src.height(); y++)
        {
            oldColor = QColor(src.pixel(x,y));

            r = oldColor.red();
            g = oldColor.green();
            b = oldColor.blue() + delta;


            b = qBound(0, b, 255);

            src.setPixel(x,y,qRgba(r,g,b,oldColor.alpha()));
        }
    }
     qDebug()<<"cool ends";
     return src;
     //changeImage();
}

Cool::Cool(const QImage &arg, int delta) : BaseFilter(arg, delta) {}