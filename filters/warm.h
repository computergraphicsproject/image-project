#ifndef WARM_H
#define WARM_H
#include "basefilter.h"

class Warm : public BaseFilter
{
	Q_OBJECT
	Q_CLASSINFO("adjustable", "true")

	virtual QImage filteredImage(const QImage &arg, int delta = 0) const override;
public:
	Q_INVOKABLE Warm(const QImage &arg, int delta = 0);
	
};

#endif // WARM_H
